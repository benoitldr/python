from marinamele_sieve_atkin import atkin
import itertools

def numbers_perm(num):
    lst_digits = [int(i) for i in str(num)]
    lst_perm = list(itertools.permutations(lst_digits))
    return [int("".join(map(str,i))) for i in lst_perm]

def is_circular_prime(p,primes):
    lst = numbers_perm(p)
    for i in lst:
        if not(i in primes):
            return False
    return True

def e35_circular_primes(n):
    primes = atkin(n)
    lst_circular = []
    count = 0
    for i in primes:
        if is_circular_prime(i,primes) and not(i in lst_circular):
            lst_circular.append(i)
            count += 1
    return lst_circular
