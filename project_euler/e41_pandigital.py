#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 29 20:00:26 2022

@author: ben
"""

from sympy import isprime

def ispandigital(n):
    s = str(n)
    r = True
    for j in range(1, len(str(n)) + 1):
        if s.count(str(j))==1:
            r = True
        else:
            return False
    return r

primes = [i for i in range(10**9) if isprime(i) and i>10**8]
for j in primes:
    if ispandigital(j):
        res = j
        print(res)