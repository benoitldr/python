from fact import fact
def is_curious_number(n):
    """is_curious_number

    :param n:
    """
    n_str = str(n)
    s_curious = 0
    for char in n_str:
        s_curious += fact(int(char))
    return s_curious == n

def e34_digital_factorial(n):
    """e34_digital_factorial
    :param n:
    """
    sum_curious = 0
    for i in range(3,n):
        if is_curious_number(i):
            sum_curious = sum_curious + i
    return sum_curious
