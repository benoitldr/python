from sympy import sieve,primerange,isprime
def is_truncatable_primes(n):
    s = str(n)
    for i in range(len(s)):
        testd = int(s[i:])
        if i < len(s) - 1:
            testg = int(s[:-i-1])
            if not(isprime(testg)):
                return False
        if not(isprime(testd)):
            return False
    return True

def e37_truncatable_primes(n):
    count = 0
    primes = [p for p in primerange(10,n)]
    lst = []
    for p in primes:
        if is_truncatable_primes(p):
            lst.append(p)
    return lst


