#!/usr/bin/env python3
"""
File: e44_pentagon_numbers.py
Author: ben
Email: ben@gresille.org
Github: https://gitlab.com/benoitldr
Description: Project Euler : problem 44
"""


def p(n):
    return n * (3 * n - 1) / 2

def solve(n):
    lp = [p(i) for i in range(1,n+1)]
    m = 10**99
    c = [1,1]
    for i in range(1,n+1):
        for j in range(1,n+1):
            if i != j:
                d = p(i)-p(j)
                s = p(i)+p(j)
                if d in lp and s in lp and d < m:
                    c = [i,j]
                    m = d
    print(m)
    print(c)
                    