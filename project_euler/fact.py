def fact(n):
    """fact

    :param n:
    """
    i = 1
    res = 1
    while i <= n:
        res = res * i
        i = i + 1
    return res
