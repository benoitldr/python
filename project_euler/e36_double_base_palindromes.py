def dec_to_bin(n):
    """dec_to_bin

    :param n:
    """
    return int(bin(n)[2:])

def is_palindrome_str(s):
    """is_palindrome_str

    :param s:
    """
    l = len(s)
    for i in range(l // 2):
        if not(s[i] == s[-1-i]):
            return False
    return True

def is_db_palindrome(n):
    """is_db_palindrome

    :param n:
    """
    binary = dec_to_bin(n)
    s_binary = str(binary)
    if is_palindrome_str(str(n)) and is_palindrome_str(s_binary):
        return True
    else:
        return False

def double_base_palindromes(n):
    """double_base_palindromes

    :param n:
    """
    lst_palindromes = []
    for i in range(1,n+1):
        if is_db_palindrome(i):
            lst_palindromes.append(i)
    return lst_palindromes
