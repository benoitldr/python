def pandigital(p,n):
    res = ""
    for i in range(1,n+1):
        res = res + str(i * p)
    return int(res)

def search_pandigital_multiple(n,m):
    res = 0
    c = [0,0]
    for i in range(1,n+1):
        for j in range(1,m+1):
            x = pandigital(i,j)
            if x > 999999999:
                break
            else:
                if x > res:
                    res = x
                    c = [i,j]
    return (res,c)
