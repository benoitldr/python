#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 25 00:05:07 2022

@author: ben
"""

from random import randint


def devinette(n):
    x = randint(1,n)
    c = 1
    mi = 1
    ma = n
    e = (mi + ma) // 2
    print(x)
    while e != x:
        print(e)
        if e < x:
            print("plus grand")
            mi = e + 1
        else:
            print("plus petit")
            ma = e - 1
        e = (mi + ma) // 2
        c = c + 1
    print(f"gagné en {c} coups")
    print(e)
    