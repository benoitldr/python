#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 01:56:10 2022

@author: ben
"""

n = int(input())

ltm = 0

for i in range(n):
    l = str(input())
    if len(l) > ltm:
        print(l)
        ltm = len(l)
    