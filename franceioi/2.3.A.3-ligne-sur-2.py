#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 01:47:28 2022

@author: ben
"""

nl = int(input())

text = []

for i in range(nl):
    text.append(str(input()))

for l in range(nl):
    if l % 2 == 0:
        print(text[l])