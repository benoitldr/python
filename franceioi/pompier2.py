#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 01:12:13 2022

@author: ben
"""

def lire_zone():
    xmin = int(input())
    xmax = int(input())
    ymin = int(input())
    ymax = int(input())
    return {'xmin': xmin, 'xmax':xmax, 'ymin': ymin, 'ymax':ymax}


def main():
    """docstring for main"""
    nb_paires_zones = int(input())
    l_zones = []
    for zone in range(nb_paires_zones):
        z1 = lire_zone()
        z2 = lire_zone()
        inter(z1, z2)

def inter(z1, z2):
    if z1['xmax'] <= z2['xmin'] or z1['xmin'] >= z2['xmax'] or z1['ymax'] <= z2['ymin'] or z1['ymin'] >= z2['ymax']:
        print("NON")
    else:
        print("OUI")

def mytest():
    z1 = {'xmin': 1, 'xmax':6, 'ymin': 1, 'ymax':5}
    z2 = {'xmin': 4, 'xmax':9, 'ymin': 3, 'ymax':8}
    inter(z1, z2)
    z3 = {'xmin': 1, 'xmax':2, 'ymin': 1, 'ymax':2}
    z4 = {'xmin': 2, 'xmax':4, 'ymin': 6, 'ymax':8}
    inter(z3,z4)


if __name__ == '__main__':
    main()
