#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 01:59:48 2022

@author: ben
"""

LMAXM = 100

l = str(input())

l = l.split(" ")
nl = int(l[0])
nm = int(l[1])

nbmot = [0 for i in range(101)]

for i in range(nl):
    p = str(input())
    tp = p.split(" ")
    for m in tp:
        nbmot[len(m)] += 1

for j in range(LMAXM):
    if nbmot[j] > 0:
        print("{} : {}".format(j, nbmot[j]))