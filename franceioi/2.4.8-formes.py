#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 03:04:46 2022

@author: ben
"""

def ligne(n, c):
    l = ""
    for i in range(n):
        l = l + c
    print(l)
    
    
def rectangle(l, c):
    for y in range(l):
        if y == 0 or y == l - 1:
            ligne(c,"#")
        elif c == 1:
            print("#")
        else:
            print("#" + (c - 2) * ' ' + "#")

def triangle(n):
    for x in range(n):
        if x == 0:
            print("@")
        elif x == n - 1:
            ligne(n,"@")
        else:
            print("@" + (x-1)*" " + "@")
            