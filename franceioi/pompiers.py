#!/usr/bin/env python3
class Zone:
    def __init__(self, xmin, xmax, ymin, ymax):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax

    def inters(self, z):
        for x in range(z.xmin, z.xmax + 1):
            for y in range(z.ymin, z.xmax + 1):
                if self.xmin <= x <= self.xmax and self.ymin <= y <= self.ymax:
                    print("OUI")
                    return
        print("NON")

def lire_zone():
    xmin = int(input())
    xmax = int(input())
    ymin = int(input())
    ymax = int(input())
    z = Zone(xmin, xmax, ymin, ymax)
    return z


def main():
    """docstring for main"""
    nb_paires_zones = int(input())
    l_zones = []
    for zone in range(nb_paires_zones):
        z1 = lire_zone()
        z2 = lire_zone()
        l_zones.append((z1,z2))
    for cz in l_zones:
        cz[0].inters(cz[1])


def mytest():
    z1 = Zone(1, 6, 1, 5)
    z2 = Zone(4, 9, 3, 8)
    z1.inters(z2)
    z3 = Zone(1, 2, 1, 2)
    z4 = Zone(1, 2, 1, 2)
    z3.inters(z4)


if __name__ == '__main__':
    mytest()
    main()
