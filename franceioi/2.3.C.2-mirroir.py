#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 02:21:46 2022

@author: ben
"""

nl = int(input())

for l in range(nl):
    t = str(input())
    nt = ""
    for c in reversed(t):
        nt = nt + c
    print(nt)