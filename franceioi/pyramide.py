#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  8 13:38:23 2022

@author: ben
"""

from math import sqrt

def pyramide_rec(h):
    if h == 0:
        return 0
    elif h == 1:
        return 1
    else:
        return pyramide_rec(h-1)+h**2

def pyramide(n):
    h = 1
    np = 1
    while np <= n:
        h = h + 1
        np = np + h**2
    np = np - h**2
    h = h - 1
    print(h)
    print(np)

pyramide(20)
pyramide(26042)
#n = int(input())
#pyramide(n)
