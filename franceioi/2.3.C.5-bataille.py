#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 02:27:57 2022

@author: ben
"""
NC = 52

j1 = str(input())
j2 = str(input())

ng1 = 0
ng2 = 0
ne = 0

for i in range(min(len(j1),len(j2))):
    if j1[i] > j2[i]:
        ng1 += 1
        break
    elif j1[i] < j2[i]:
        ng2 += 1
        break
    else:
        ne += 1
       

if ng1 > ng2 or (ng1 == ng2 and len(j2) < len(j1)):
    print(1)
elif ng1 < ng2 or (ng1 == ng2 and len(j2) > len(j1)):
    print(2)
else:
    print("=")
    
print(ne)