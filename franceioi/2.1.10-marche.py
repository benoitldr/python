#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 01:13:28 2022

@author: ben
"""

nbe = int(input())

tpos = [0 for j in range(nbe)]
te = []

for e in range(nbe):
    n = int(input())
    te.append(n)

for i in range(nbe):
    tpos[te[i]] = i

for pos in tpos:
    print(pos)