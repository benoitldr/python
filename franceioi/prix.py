#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 02:08:41 2022

@author: ben
"""

def prix(t1, t2, prix):
    nprix = round(prix / (1+t1/100) * (1+t2/100),2)
    return nprix

print(prix(5.5,19.6,24.9))
print(prix(21.5,21.5,19.99))