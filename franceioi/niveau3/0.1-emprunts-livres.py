#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 14:51:58 2022

@author: ben
"""


s = str(input()).split(" ")
nbLivres = int(s[0])
nbJours = int(s[1])
tdispo = [0 for i in range(nbLivres)]
for ij in range(nbJours):
    nbClients = int(input())
    for ic in range(nbClients):
        s = str(input()).split(" ")
        iLivre = int(s[0])
        duree = int(s[1])
        if tdispo[iLivre] == 0:
            tdispo[iLivre] = duree 
            print(1)
        else:
            tdispo[iLivre] -= 1
            print(0)
        
                