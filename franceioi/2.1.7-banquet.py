#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 00:31:25 2022

@author: ben
"""

import pyautogui

npos = int(input())
nchg = int(input())

def lire_pos(npos):
    tpos = []
    for i in range(npos):
        tpos.append(int(input()))
    return tpos

def chg(tpos, nc):
    for j in range(nc):
        pos1 = int(input())
        pos2 = int(input())
        pos[pos1], pos[pos2] = pos[pos2], pos[pos1]
    return pos
   
pos = lire_pos(npos) 
t = chg(pos, nchg)
for p in t:
    print(p)