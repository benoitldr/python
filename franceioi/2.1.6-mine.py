#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 00:13:03 2022

@author: ben
"""

#nbdep = int(input())

def tabd(n):
    tabd = []
    for i in range(n):
        tabd.append(int(input()))
    return tabd

def sortie(t):
    for d in reversed(t):
        if d == 1:
            print(2)
        elif d == 2:
            print(1)
        elif d == 3:
            print(3)
        elif d == 4:
            print(5)
        elif d == 5:
            print(4)
            
def test():
    t = [1, 3, 2, 4, 4, 5]
    sortie(t)
    
test()